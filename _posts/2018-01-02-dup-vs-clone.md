---
layout: post
title: "Ruby notes: Отличия dup от clone"
date: 2018-01-20 16:20:00 +0300
sitemap: false
keywords: ruby dup clone notes
---

В Ruby есть несколько способов создать дубликат существующего объекта. Первый - метод `clone`, второй - `dup`. Оба метода 
определены в классе `Object`, следовательно они есть у любого объекта. Ниже приведены ключевые отличия между ними.
### #Object.dup
* не копирует внутреннее состояние объекта(`extended` модули, singleton методы и константы не будут скопированы)
```ruby
	obj = Object.new
	obj.extend(Enumerable)
	obj.dup.singleton_methods # returns nothing
```
* размораживет `freeze` объекты
```ruby
	class Foo
	    attr_accessor :bar
	end
	o = Foo.new
	o.freeze
	o.dup.bar = 10 # success
```
* в Rails создает копию объекта без `id`(вызывая у объекта метод `save` можно сохранить НОВЫЙ объект в БД)
```ruby
	category2 = category.dup
	#=> #<Category id: nil, name: "Favorites"> 
```

### #Object.clone
* полностью копирует внутреннее состояние объекта
```ruby
	# extended modules will be cloned
	obj = Object.new
	obj.extend(Enumerable)
	obj.clone.singleton_methods # returns Enumerable's class methods
	
	# singleton methods will be cloned too
	obj2 = Object.new
	def obj2.foo
	  42
	end

	obj.clone.foo # return 42
```
* не размораживает `freeze` объекты
* в Rails создает копию объекта с `id`(если вызвать `save` на объекте, можно ОБНОВИТЬ существующий объект в БД)
```ruby
	category2 = category.сlone
	#=> #<Category id: 1, name: "Favorites"> 
```