---
layout: post
title: "Немного кастомизации в Firefox"
date: 2018-01-29 12:11 +0300
sitemap: true
---

Вчера поставил себе новый Firefox и всё вроде бы ничего, но анимация при переходе в fullscreen в видео просто бесит. Да и еще этот банер вылазит. Вообще не торт. Чтобы избавится от этих "прекрасностей", нужно поправить парочку переменных в `about:config`.

Убираем анимацию:
```
full-screen-api.transition-duration.enter: 0 0
full-screen-api.transition-duration.leave: 0 0 
```

Убираем баннер с уведомлением:
```
full-screen-api.warning.timeout: 0
```

Теперь все как у людей 😃


